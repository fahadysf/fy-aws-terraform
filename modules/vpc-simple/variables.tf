
variable "vpc_cidr" {
  type        = string
  default     = "10.200.0.0/18"
  description = "CIDR for FY Test VPC"
}

variable "tenancy" {
  type        = string
  default     = "default"
  description = ""
}

variable "enable_dns_support" {
  default = true
}

variable "enable_dns_hostnames" {
  default = true
}

variable "vpc_name" {
  type        = string
  default     = "fy-test-vpc"
  description = ""
}

variable "public_cidr" {}
variable "private_cidr" {}

// variable "private_az" {}
// variable "public_az" {}
