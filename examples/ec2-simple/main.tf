
provider "aws" {
  shared_credentials_file = "/Users/fyousuf/.aws/credentials"
  profile                 = "default"
  region                  = var.aws_region
}

module "vpc" {
  source       = "../../modules/vpc-simple"
  vpc_name     = "fy-test-vpc"
  vpc_cidr     = "10.100.0.0/18"
  public_cidr  = "10.100.1.0/24"
  private_cidr = "10.100.2.0/24"
}

resource "aws_key_pair" "fy-aws-keypair" {
  key_name   = "fy-aws-keypair"
  public_key = file("${path.root}/fy-aws-keypair.pub")
}

resource "aws_network_interface" "management" {
  subnet_id       = module.vpc.subnet_public_id
  security_groups = [ aws_security_group.ec2-sg.id ]
}

resource "aws_network_interface" "internal" {
  subnet_id         = module.vpc.subnet_private_id
  security_groups   = [ aws_security_group.ec2-sg.id ]
}

resource "aws_eip" "nat_gw_eip_1" {
  vpc = true
}

resource "aws_instance" "public-ec2" {
  ami                         = var.ami_id
  instance_type               = var.instance_type
  key_name                    = "fy-aws-keypair"

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.management.id
  }

  network_interface {
    device_index         = 1
    network_interface_id = aws_network_interface.internal.id
  }

  tags = {
    Name  = "fy-test-swall-nsv-1"
    Owner = "fyousuf"
  }

  depends_on = [module.vpc.vpc_id, module.vpc.igw_id]
}


resource "aws_security_group" "ec2-sg" {
  name        = "security-group"
  description = "allow inbound access to the Application task from NGINX"
  vpc_id      = module.vpc.vpc_id
  ingress {
    protocol    = "tcp"
    from_port   = 0
    to_port     = 22
    cidr_blocks = ["188.55.91.162/32"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 0
    to_port     = 443
    cidr_blocks = ["188.55.91.162/32"]
  }
  ingress {
    protocol    = "icmp"
    from_port   = -1
    to_port     = -1
    cidr_blocks = ["188.55.91.162/32"]
  }
  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
