
variable "aws_region" {
  default = "eu-west-1"
}
variable "ec2_count" {
  default = "1"
}

variable "ami_id" {
  // Sonicwall NSv 7.0.1 (eu-west-1)
  default = "ami-0564ff3162d987ce1"
  // Ubuntu 20.04 AMI (eu-west-1)
  //default = "ami-090f70058ee827c32"
}

variable "instance_type" {
  // For Sonicwall NSv 7.0.1
  default = "c5.large"
}

// variable "subnet_id" {}

